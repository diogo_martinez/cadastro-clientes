**Desafio Builders - Aplicação de cadastro de clientes: Processo Criativo**

O plano para esse desafio foi construir a api de forma mais simples possível, utilizando ao máximo os recursos do próprio Spring. Também fez parte do plano, uma infraestrutura cloud para executar o ci/cd da app.

---

## Arquitetura 

Resumindo a arquitetura:

1. Java com Spring Boot 2
2. Bitbucket/Bitbucket Pipelines para repositorio e ci/cd
3. DockerHub para armazenamento da versão
4. Terraform como ferramenta de IaC
5. Aws Cloud para hospedar a app.

---

## Definições da arquitetura da App

Foi utilizado o Spring boot 2 e seus *plugins* para construir toda a app. A escolha foi basicamente porque o spring boot, hoje, é uma das ferramentas mais usadas para desenvolvimento java.

Nas camadas da aplicação, optei por não utilizar um *Service*, visto que a aplicação era bem simples. Com isso, o Controller pode ter ganhado um pouquinho mais de complexidade, pois não foi possível fazer alguma inteligência mais orientada a exception, por exemplo.

A escolha do H2 em memoria, foi porque tentei diminuir algumas complexidades. Quanto mais eu escrevia, mais coisas queria fazer, mas o tempo foi um peso para alguns cortes de escopo. O mesmo vale para a inclusão de cache na app.

Utilizei uma camada simples de Basic Auth para aplicação, já utilizando spring security.

Como documentação da aplicação foi utilizado o Swagger com a notação da open api 2. Além disso, a documentação integra os testes de integração, onde conseguimos validar se o que está documentado, é o que está implementado.

Para não expor a nossa Entidade para a camada de visualização, objetos distintos e mappers foram utilizados para transportar os dados entre as camadas.

Para Log, foi utilizado um formato indexável, para o melhor aproveitamento em ferramentas de visualização como o Elastic Stack.

Para a criação da imagem da aplicação, utilizei como versionamento/tag o commit abreviado, referente a mudança.

O processo de ci/cd é feito após cada commit, rodando os testes unitarios, integração, geração da imagem docker e deploy na nuvem.
> A geração de imagem é feita pelo próprio maven

## Definições da arquitetura de infra

Utilizei o Aws Ecs com Fargate, onde conseguimos ter uma experiência próxima a um Paas, com Alta disponibilidade, escalabilidade, monitoramento/logging e deploy (0 downtime).

Toda a infra está codificada no diretório **/src/main/devops**. Foi utilizado o Hashcorp Terraform para tal.

Resumindo um pouco das caracteristicas:
- Alta Disponibilidade: Utilização de 2 redes em datacenters diferentes para garantir o funcionamento;
- Estratégia de escalabilidade: Politica de escalabilidade utilizando recurso de máquina para definir se um novo container deve ou não iniciado;
- Monitoramento e Logging: Utilização dos monitoramentos padrão da Aws e do CloudWatch logs para centralizar os logs dos containers;
- Deploy: Utilização de Tasks para fazer deploys de novas imagens, utilizando a estratégia padrão(geralmente round robin) do **Fargate** para não termos downtime.

---

## Não Escopo

1. Autorização e Autenticação complexa na app.
2. Utilização de Https com um dominio mais comercial para a app.
3. Banco de dados separado da aplicação.
4. Estratégias mais complexas de deploy e segurança a nivel de infra.
5. Cache diferente do padrão fornecido pelo Spring Data Jpa.
6. Separação de profiles da aplicação.
7. Performance e criação de indice nos campos de consulta.
8. Utilizar um data migration tool de mais alto nivel.

## Dados da aplicação

Acesso a app via: http://buildersapp-load-balancer-1441436983.us-east-1.elb.amazonaws.com:8080/clientes

Usuario: user  
Pass: 3a744b65-2733-4022-bcf3-4accd7ad58fc
> arquivo postman disponibilizado na raiz do projeto

