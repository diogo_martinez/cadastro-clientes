package br.com.desafiobuilders.integration;

import br.com.desafiobuilders.repository.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static com.atlassian.oai.validator.mockmvc.SwaggerValidatorMatchers.swagger;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
class ClienteControllerIntegrationTest {

    public static final String AUTH = "Basic dXNlcjozYTc0NGI2NS0yNzMzLTQwMjItYmNmMy00YWNjZDdhZDU4ZmM=";
    public static final String PAYLOAD_PUT_ERRO = "{\"nome\":\"TesteAlterado\",\"cpf\":\"11111111102\",\"dataNascimento\":\"1990-01-02\"}";
    public static final String PAYLOAD_PUT_SUCESSO = "{\"nome\":\"TesteAlt\",\"cpf\":\"11111111102\",\"dataNascimento\":\"1990-01-02\"}";
    public static final String PAYLOAD_POST_SUCESSO = "{\"nome\":\"TestePost\",\"cpf\":\"11111111140\",\"dataNascimento\":\"1990-02-02\"}";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClienteRepository repository;

    @Test
    void findAll() throws Exception {
        this.mockMvc.perform(get("/clientes")
                .header("Authorization", AUTH))
                .andExpect(jsonPath("$.content", is(notNullValue())))
                .andExpect(jsonPath("$.size", is(5)))
                .andExpect(jsonPath("$.totalPages", is(3)))
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(status().isOk())
                .andExpect(swagger().isValid("swagger-api.yml"));;
    }

    @Test
    void unauthorized() throws Exception {
        this.mockMvc.perform(get("/clientes"))
                .andExpect(status().isUnauthorized())
                .andExpect(swagger().isValid("swagger-api.yml"));;
    }

    @Test
    void findByCPF() throws Exception {
        this.mockMvc.perform(get("/clientes/cpf/11111111101")
                .header("Authorization", AUTH))
                .andExpect(jsonPath("$.nome", is("Teste1")))
                .andExpect(jsonPath("$.cpf", is("11111111101")))
                .andExpect(jsonPath("$.dataNascimento", is("1990-01-01")))
                .andExpect(jsonPath("$.idade", is(31)))
                .andExpect(status().isOk())
                .andExpect(swagger().isValid("swagger-api.yml"));;
    }

    @Test
    void findByCPF_notFound() throws Exception {
        notFoundTest("/clientes/cpf/11111111120");
    }

    @Test
    void findByCPF_internalServerError() throws Exception {
        this.mockMvc.perform(get("/clientes/cpf/11111111110")
                .header("Authorization", AUTH))
                .andExpect(status().isInternalServerError())
                .andExpect(swagger().isValid("swagger-api.yml"));
    }

    @Test
    void findByNome_notFound() throws Exception {
        notFoundTest("/clientes/nome/rob");
    }

    private void notFoundTest(String url) throws Exception {
        this.mockMvc.perform(get(url)
                .header("Authorization", AUTH))
                .andExpect(status().isNotFound())
                .andExpect(swagger().isValid("swagger-api.yml"));

    }

    @Test
    void findByNome() throws Exception {
        this.mockMvc.perform(get("/clientes/nome/Teste")
                .header("Authorization", AUTH))
                .andExpect(jsonPath("$.content", is(notNullValue())))
                .andExpect(jsonPath("$.size", is(5)))
                .andExpect(jsonPath("$.totalPages", is(3)))
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(status().isOk())
                .andExpect(swagger().isValid("swagger-api.yml"));;
    }

    @Test
    void findById_notFound() throws Exception {
        notFoundTest("/clientes/25");
    }

    @Test
    void findById() throws Exception {
        this.mockMvc.perform(get("/clientes/1")
                .header("Authorization", AUTH))
                .andExpect(jsonPath("$.nome", is("Teste1")))
                .andExpect(jsonPath("$.cpf", is("11111111101")))
                .andExpect(jsonPath("$.dataNascimento", is("1990-01-01")))
                .andExpect(jsonPath("$.idade", is(31)))
                .andExpect(status().isOk())
                .andExpect(swagger().isValid("swagger-api.yml"));;
    }


    @Test
    void atualizaCliente_badRequest() throws Exception {
        this.mockMvc.perform(put("/clientes/2").with(csrf())
                .header("Authorization", AUTH)
                .content(PAYLOAD_PUT_ERRO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isBadRequest())
                .andExpect(swagger().isValid("swagger-api.yml"));
    }

    @Test
    void atualizaCliente_thenSuccess() throws Exception {
        this.mockMvc.perform(put("/clientes/2").with(csrf())
                .header("Authorization", AUTH)
                .content(PAYLOAD_PUT_SUCESSO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isNoContent())
                .andExpect(swagger().isValid("swagger-api.yml"));
        Assertions.assertEquals("TesteAlt", repository.findById(2L).get().getNome());
    }

    @Test
    void whenPostClientes_thenSuccess() throws Exception {
        this.mockMvc.perform(post("/clientes").with(csrf())
                .header("Authorization", AUTH)
                .content(PAYLOAD_POST_SUCESSO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isCreated())
                .andExpect(swagger().isValid("swagger-api.yml"));
        Assertions.assertEquals("TestePost", repository.findById(12L).get().getNome());
    }
}
