package br.com.desafiobuilders.mapper;

import br.com.desafiobuilders.to.ClienteRequestTO;
import br.com.desafiobuilders.entity.ClienteEntity;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ClienteEntityMapperTest {



    @Test
    public void whenFromClienteRequest_thenSuccess(){
        EasyRandom generator = new EasyRandom();
        ClienteRequestTO request = generator.nextObject(ClienteRequestTO.class);
        ClienteEntity entity =  ClienteEntityMapper.fromClienteRequest(request);

        Assertions.assertEquals(request.getCpf(),entity.getCpf());
        Assertions.assertEquals(request.getNome(),entity.getNome());
        Assertions.assertEquals(request.getDataNascimento(),entity.getDataNascimento());

    }

    @Test
    public void whenFromClienteRequestToClienteEntity_thenSuccess(){
        EasyRandom generator = new EasyRandom();
        ClienteRequestTO request = generator.nextObject(ClienteRequestTO.class);
        ClienteEntity entity = new ClienteEntity();

        ClienteEntityMapper.fromClienteRequestToClienteEntity(request, entity);

        Assertions.assertEquals(request.getCpf(),entity.getCpf());
        Assertions.assertEquals(request.getNome(),entity.getNome());
        Assertions.assertEquals(request.getDataNascimento(),entity.getDataNascimento());

    }

    @Test
    public void whenFromClienteRequestToClienteEntityNotEmpty_thenSuccess(){
        EasyRandom generator = new EasyRandom();
        ClienteRequestTO request = generator.nextObject(ClienteRequestTO.class);
        ClienteEntity entity = generator.nextObject(ClienteEntity.class);
        Long id = entity.getId();

        ClienteEntityMapper.fromClienteRequestToClienteEntity(request, entity);

        Assertions.assertEquals(request.getCpf(),entity.getCpf());
        Assertions.assertEquals(request.getNome(),entity.getNome());
        Assertions.assertEquals(request.getDataNascimento(),entity.getDataNascimento());
        Assertions.assertEquals(id,entity.getId());

    }
}
