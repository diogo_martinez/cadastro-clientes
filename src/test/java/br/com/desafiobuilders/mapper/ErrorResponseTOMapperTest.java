package br.com.desafiobuilders.mapper;

import br.com.desafiobuilders.to.ErrorResponseTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
public class ErrorResponseTOMapperTest {

    @Test
    public void fromPayloadErros(){
        FieldError erro1 = new FieldError("teste","cpf", "nulo");
        FieldError erro2 = new FieldError("teste","nome", "nulo");
        List<FieldError> lista = new ArrayList<>();
        lista.add(erro1);
        lista.add(erro2);
        ErrorResponseTO to = ErrorResponseTOMapper.fromPayloadErros(lista);

        Assertions.assertEquals(2, to.erros.size());
        Assertions.assertEquals("cpf", to.erros.get(0).getCampo());
        Assertions.assertEquals("nome", to.erros.get(1).getCampo());
    }
}
