package br.com.desafiobuilders.controller;

import br.com.desafiobuilders.entity.ClienteEntity;
import br.com.desafiobuilders.repository.ClienteRepository;
import br.com.desafiobuilders.to.ClienteRequestTO;
import br.com.desafiobuilders.to.ClienteResponseTO;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)

public class ClienteControllerTest {

    private static final Long ID = 1L;
    private static String CPF = "11111111111";

    @InjectMocks
    private ClienteController controller;

    @Mock
    private ClienteRepository repository;

    private EasyRandom generator = new EasyRandom();


    @Test
    public void whenFindAllClientes_thenSuccess(){
        when(repository.findAll(any(Pageable.class))).thenReturn(mock(Page.class));
        ResponseEntity response = controller.findAllClientes(PageRequest.of(0, 5));
        Assertions.assertEquals(200, response.getStatusCodeValue());

    }

    @Test
    public void whenFindAllClientes_thenFail(){
        when(repository.findAll(any(Pageable.class))).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class, () -> {
            controller.findAllClientes(PageRequest.of(0, 5));
        });

    }

    @Test
    public void whenFindByCpf_thenSuccess(){
        EasyRandom generator = new EasyRandom();
        ClienteEntity entity = generator.nextObject(ClienteEntity.class);

        when(repository.findByCpf(CPF)).thenReturn(Optional.of(entity));

        ResponseEntity response = controller.findByCPF(CPF);
        Assertions.assertEquals(200, response.getStatusCodeValue());

    }

    @Test
    public void whenFindByCpf_thenFail(){

        when(repository.findByCpf(CPF)).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class, () -> {
            controller.findByCPF(CPF);
        });

    }

    @Test
    public void whenCriarCliente_thenSuccess(){

        ClienteEntity entity = generator.nextObject(ClienteEntity.class);

        when(repository.save(any(ClienteEntity.class))).thenReturn(entity);

        ResponseEntity response = controller.criarCliente(mock(ClienteRequestTO.class));
        Assertions.assertEquals(201, response.getStatusCodeValue());

    }

    @Test
    public void whenCriarCliente_thenFail(){

        when(repository.save(any(ClienteEntity.class))).thenThrow(RuntimeException.class);
        assertThrows(RuntimeException.class, () -> {
            controller.criarCliente(mock(ClienteRequestTO.class));
        });

    }

    @Test
    public void whenAtualizarCliente_thenSuccess(){

        ClienteEntity entityOld = generator.nextObject(ClienteEntity.class);
        entityOld.setId(ID);
        ClienteRequestTO requestTO = generator.nextObject(ClienteRequestTO.class);

        when(repository.findById(ID)).thenReturn(Optional.of(entityOld));

        when(repository.save(any(ClienteEntity.class))).thenReturn(mock(ClienteEntity.class));

        ResponseEntity response = controller.atualizarCliente(requestTO,ID);
        Assertions.assertEquals(204, response.getStatusCodeValue());
        Assertions.assertEquals(entityOld.getId(), ID);
        Assertions.assertEquals(entityOld.getCpf(), requestTO.getCpf());
        Assertions.assertEquals(entityOld.getNome(), requestTO.getNome());
        Assertions.assertEquals(entityOld.getDataNascimento(), requestTO.getDataNascimento());

    }

}
