# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "buildersapp_log_group" {
  name              = "/ecs/builders-app"
  retention_in_days = 1

  tags = {
    Name = "buildersapp-log-group"
  }
}

resource "aws_cloudwatch_log_stream" "buildersapp_log_stream" {
  name           = "buildersapp-log-stream"
  log_group_name = aws_cloudwatch_log_group.buildersapp_log_group.name
}