provider "aws" {
  version = "~> 2.0"
  region = var.aws_region
  profile = "tfuser"
}

# backend.tf
terraform {
  required_version = ">= 0.12"

  backend "s3" {
    bucket  = "terraform-desafio-builders"
    key     = "terraform.tfstate"
    profile = "tfuser"
  }
}