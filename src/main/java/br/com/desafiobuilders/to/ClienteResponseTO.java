package br.com.desafiobuilders.to;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ClienteResponseTO {

    private Long id;

    private String nome;

    private String cpf;

    private LocalDate dataNascimento;

    public Integer getIdade() {
        return Long.valueOf(ChronoUnit.YEARS.between(dataNascimento, LocalDate.now())).intValue();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }
}
