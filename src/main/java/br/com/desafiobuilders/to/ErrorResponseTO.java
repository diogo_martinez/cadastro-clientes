package br.com.desafiobuilders.to;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class ErrorResponseTO {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<ErrorTO> erros;

}
