package br.com.desafiobuilders.to;

import javax.validation.constraints.Size;
import java.time.LocalDate;

public class ClienteRequestTO {
    @Size(min = 3, max = 10)
    private String nome;

    @Size(min = 11, max = 11, message = "cpf invalido")
    private String cpf;

    private LocalDate dataNascimento;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
