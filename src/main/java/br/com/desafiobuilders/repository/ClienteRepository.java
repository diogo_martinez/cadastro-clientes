package br.com.desafiobuilders.repository;

import br.com.desafiobuilders.entity.ClienteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface ClienteRepository extends PagingAndSortingRepository<ClienteEntity, Long> {

    Optional<ClienteEntity> findByCpf(@Param("cpf") String cpf);
    Page<ClienteEntity> findByNomeContaining(@Param("nome") String nome, Pageable pageable);
}
