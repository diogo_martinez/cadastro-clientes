package br.com.desafiobuilders.contants;

public class LogConstants {

    private LogConstants(){}

    public static final String KEY_CPF = "cpf";
    public static final String KEY_HTTP_STATUS = "httpStatus";
    public static final String OP_CONSULTAR_CPF = "consultarCpf";
    public static final String KEY_NOME = "nome";
    public static final String OP_CONSULTAR_NOME = "consultarNome";
    public static final String KEY_ID = "id";
    public static final String OP_CONSULTAR_ID = "consultarId";
    public static final String KEY_REQUEST_BODY = "requestBody";
    public static final String KEY_RESPONSE_BODY = "responseBody";
    public static final String OP_CADASTRAR_CLIENTE = "cadastrarCliente";
    public static final String OP_ATUALIZAR_CLIENTE = "atualizarCliente";
    public static final String KEY_PAGINA = "pagina";
    public static final String OP_CONSULTAR_TODOS = "consultarTodosClientes";
}
