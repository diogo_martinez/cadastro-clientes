package br.com.desafiobuilders.mapper;


import br.com.desafiobuilders.to.ErrorResponseTO;
import br.com.desafiobuilders.to.ErrorTO;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;


public class ErrorResponseTOMapper {

    private ErrorResponseTOMapper(){}

    public static ErrorResponseTO fromPayloadErros(List<FieldError> erros) {
        List<ErrorTO> errosMap = new ArrayList<>();
        erros.forEach(erro -> {
            switch (erro.getField()) {
                case "cpf":
                    errosMap.add(
                            new ErrorTO("cpf", "cpf invalido"));
                    break;
                case "nome":
                    errosMap.add(new ErrorTO("nome", "Nome deve ter de 3 a 10 caracteres"));
                    break;
                default:
            }
        });
        ErrorResponseTO to = new ErrorResponseTO();
        to.erros = errosMap;


        return to;
    }

}
