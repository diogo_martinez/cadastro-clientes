package br.com.desafiobuilders.mapper;

import br.com.desafiobuilders.to.ClienteRequestTO;
import br.com.desafiobuilders.entity.ClienteEntity;
import org.springframework.beans.BeanUtils;

public class ClienteEntityMapper {

    private ClienteEntityMapper(){}

    public static ClienteEntity fromClienteRequest(ClienteRequestTO request){
        ClienteEntity entity = new ClienteEntity();
        BeanUtils.copyProperties(request, entity);
        return entity;
    }

    public static void fromClienteRequestToClienteEntity(ClienteRequestTO request, ClienteEntity clienteEntity) {
        BeanUtils.copyProperties(request, clienteEntity);
    }
}
