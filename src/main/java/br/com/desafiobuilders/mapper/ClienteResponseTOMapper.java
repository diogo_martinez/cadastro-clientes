package br.com.desafiobuilders.mapper;

import br.com.desafiobuilders.to.ClienteResponseTO;
import br.com.desafiobuilders.entity.ClienteEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;


public class ClienteResponseTOMapper {

    private ClienteResponseTOMapper(){}

    public static Page<ClienteResponseTO> fromListClienteEntity(Page<ClienteEntity> clientes){
        return clientes.map(entity -> {
                ClienteResponseTO dto = new ClienteResponseTO();
                dto.setId(entity.getId());
                dto.setCpf(entity.getCpf());
                dto.setDataNascimento(entity.getDataNascimento());
                dto.setNome(entity.getNome());
                return dto;
        });
    }

    public static ClienteResponseTO fromClienteEntity(ClienteEntity clienteEntity) {
        ClienteResponseTO response = new ClienteResponseTO();
        BeanUtils.copyProperties(clienteEntity, response);
        return response;
    }
}
