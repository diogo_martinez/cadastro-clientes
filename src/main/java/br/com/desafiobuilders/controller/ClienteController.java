package br.com.desafiobuilders.controller;

import br.com.desafiobuilders.contants.LogConstants;
import br.com.desafiobuilders.to.ClienteRequestTO;
import br.com.desafiobuilders.to.ClienteResponseTO;
import br.com.desafiobuilders.to.ErrorResponseTO;
import br.com.desafiobuilders.entity.ClienteEntity;
import br.com.desafiobuilders.mapper.ClienteEntityMapper;
import br.com.desafiobuilders.mapper.ClienteResponseTOMapper;
import br.com.desafiobuilders.mapper.ErrorResponseTOMapper;
import br.com.desafiobuilders.repository.ClienteRepository;
import net.logstash.logback.marker.LogstashMarker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import static net.logstash.logback.marker.Markers.append;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    private Logger logger = LoggerFactory.getLogger(ClienteController.class);

    @Autowired
    private ClienteRepository repository;

    @GetMapping()
    public ResponseEntity<Page<ClienteResponseTO>> findAllClientes(Pageable pageable) {
        LogstashMarker logMarker = append(LogConstants.KEY_PAGINA, pageable.getPageNumber());

        logger.info( logMarker, LogConstants.OP_CONSULTAR_TODOS);

        return ResponseEntity.ok(ClienteResponseTOMapper.fromListClienteEntity(repository.findAll(pageable)));
    }

    @GetMapping(path = "/cpf/{cpf:[0-9]{11}}") //cpf e um dado sensivel, nao eh o ideal passar por url.
    public ResponseEntity<ClienteResponseTO> findByCPF(@PathVariable String cpf) {
        LogstashMarker logMarker = append(LogConstants.KEY_CPF, cpf);

        Optional<ClienteEntity> optionalClienteEntity = repository.findByCpf(cpf);
        if(optionalClienteEntity.isPresent()){

            logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.OK));
            logger.info(logMarker, LogConstants.OP_CONSULTAR_CPF);

            return ResponseEntity.ok(ClienteResponseTOMapper.fromClienteEntity(optionalClienteEntity.get()));
        }

        logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.NOT_FOUND));
        logger.info(logMarker, LogConstants.OP_CONSULTAR_CPF);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping(path = "/nome/{nome}")
    public ResponseEntity<Page<ClienteResponseTO>> findByNome(@PathVariable String nome, Pageable pageable) {

        LogstashMarker logMarker = append(LogConstants.KEY_NOME, nome);

        Page<ClienteEntity> clienteEntityPage = repository.findByNomeContaining(nome, pageable);
        if(clienteEntityPage.hasContent()){

            logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.OK));
            logger.info(logMarker, LogConstants.OP_CONSULTAR_NOME);

            return ResponseEntity.ok(ClienteResponseTOMapper.fromListClienteEntity(clienteEntityPage));
        }

        logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.NOT_FOUND));
        logger.info(logMarker, LogConstants.OP_CONSULTAR_NOME);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ClienteResponseTO> findById(@PathVariable Long id) {
        LogstashMarker logMarker = append(LogConstants.KEY_ID, id);

        Optional<ClienteEntity> entityOptional = repository.findById(id);
        if(entityOptional.isPresent()){

            logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.OK));
            logger.info(logMarker, LogConstants.OP_CONSULTAR_ID);

            return ResponseEntity.ok(ClienteResponseTOMapper.fromClienteEntity(entityOptional.get()));
        }
        logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.NOT_FOUND));
        logger.info(logMarker, LogConstants.OP_CONSULTAR_ID);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<ClienteResponseTO> criarCliente(@Valid @RequestBody ClienteRequestTO request) {

        LogstashMarker logMarker = append(LogConstants.KEY_REQUEST_BODY, request);

        ClienteEntity entity = repository.save(ClienteEntityMapper.fromClienteRequest(request));

        ClienteResponseTO response = ClienteResponseTOMapper.fromClienteEntity(entity);

        logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.CREATED))
            .and(append(LogConstants.KEY_RESPONSE_BODY, response));

        logger.info(logMarker,LogConstants.OP_CADASTRAR_CLIENTE);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Void> atualizarCliente(@Valid @RequestBody ClienteRequestTO request, @PathVariable Long id) {

        LogstashMarker logMarker = append(LogConstants.KEY_REQUEST_BODY, request)
                .and(append(LogConstants.KEY_ID, id));

        Optional<ClienteEntity> optionalClienteEntity = repository.findById(id);

        if(optionalClienteEntity.isPresent()){
            ClienteEntityMapper.fromClienteRequestToClienteEntity(request, optionalClienteEntity.get());
            repository.save(optionalClienteEntity.get());

            logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.NO_CONTENT));
            logger.info(logMarker,LogConstants.OP_ATUALIZAR_CLIENTE);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        logMarker.and(append(LogConstants.KEY_HTTP_STATUS, HttpStatus.NO_CONTENT));
        logger.info(logMarker,LogConstants.OP_ATUALIZAR_CLIENTE);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    private String tratarErroGeral(Exception ex) {
       logger.error("erro",ex);

        return "Falha Interna";

    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private ErrorResponseTO tratarErroPayload(MethodArgumentNotValidException ex) {
        List<FieldError> errosValidacao = ex.getBindingResult().getFieldErrors();

        return ErrorResponseTOMapper.fromPayloadErros(errosValidacao);

    }

}
