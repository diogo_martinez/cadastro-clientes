package br.com.desafiobuilders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafiobuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafiobuildersApplication.class, args);
	}

}
